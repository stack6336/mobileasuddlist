<?php
class db {
/**
 * Подключение к БД. Возвращает PDO объект
 */

    public static function getConnection() { 
        try {
            $db = null;
            $dsn = "sqlsrv:Server={$GLOBALS['PARAM']['dbHostMSSQL']},{$GLOBALS['PARAM']['dbPortMSSQL']};Database={$GLOBALS['PARAM']['dbNameMSSQL']}";
            
            $db  = new PDO($dsn, $GLOBALS['PARAM']['dbUserMSSQL'], $GLOBALS['PARAM']['dbPassMSSQL']);
            // Используем исключения
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        } catch (PDOException $Exception) {
            //Выводим сообщение об исключении.
             var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }

        return $db;
    }


}
