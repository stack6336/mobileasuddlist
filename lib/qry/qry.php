<?php

/**
 * Created by PhpStorm.
 * User: Роман
 * Date: 12.03.2019
 * Time: 12:57
 */
class qry {

    public function __construct() {
        
    }

    /**
     * выполнение запроса
     * @param $sql
     * @param array $values
     * @return array|bool|PDOStatement
     */
    public static function queryGet($sql, $values = []) {
        $db = DB::getConnection();
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
        try {
            $result->execute($values);
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * выполнение SQL скрипта
     * @param $sql
     * @return bool
     */
    public static function queryExec($sql, $values = [],$returnID = false) {
        $db = DB::getConnection();
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
        try {
            if ($result->execute($values)) {
                if ($returnID) {
                    return $db->lastInsertId();
                }
                unset($db);
                return true;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
    }

    public static function queryInsert($dbase, $rowName, $value) {
        $db = DB::getConnection();
        $sql = 'SET DATEFORMAT dmy INSERT INTO ' . $dbase . ' (' . $rowName . ') VALUES(' . $value . ')';
        $result = $db->prepare($sql);
        try {
            ;
            if ($result->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
    }

    public static function queryExist($sql) {
        $db = DB::getConnection();
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
        try {
            $result->execute();
            if ($result->fetch(PDO::FETCH_NUM)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            var_dump($Exception->getCode() . ":" . $Exception->getMessage());
        }
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * обработка $_post $_get запросов
     * @return mixed
     */
    public static function rout() {
        if (!empty($_POST)) {
            return $_POST;
        } else {
            return $_GET;
        }
    }

    public static function logB($val) {
        $today = date("d-m-y");
        $now = date("d-m-y H:i:s");
        $p = './content/app-assets/files/Log/SQLLog/' . $today . '/';
//        filesOp::mkDirPS($p);
//        file_put_contents($p . 'log.txt', $now . PHP_EOL . '| ' . $_SESSION['dickrname'] . ' | ' . ' | ' . $_SESSION['pageN'] . ' |' . PHP_EOL . $val . PHP_EOL, FILE_APPEND);
    }

    public static function insertJSON($rout) {//API BD
        $json = json_decode($rout['data'], true);

        //print_r($json);
        //-Готовим запрос-
        //$json.nameDB
        //$json.nameTable
        //$json.nameColume
        //foreach ($json as $k=>$value) {}
        //echo $rout['nameColume'];
        switch ($rout['cmd']) {
            case "u"://Формирование нормы расходов
                foreach ($json['nameColume'] as $k => $value) {
                    $name .= $k . "='" . $value . "',";
                }
                $where = $json['where'];
                $name = substr($name, 0, -1);
                $sql = "UPDATE  " . $json['nameDB'] . ".." . $json['nameTable'] . " SET $name where $where \n";
                if (qry::queryExec($sql)) {
                    echo 'ok';
                } else {
                    echo 'error';
                };
                exit;
                break;
            case "i":
                foreach ($json['nameColume'] as $k => $value) {
                    $name .= $k . ',';
                    $val .= "'" . $value . "',";
                }
                $name = substr($name, 0, -1);
                $val = substr($val, 0, -1);
                $sql = "INSERT INTO " . $json['nameDB'] . ".." . $json['nameTable'] . " (" . $name . ") VALUES(" . $val . ")";
                if (qry::queryExec($sql)) {
                    echo 'ok';
                } else {
                    echo 'error';
                };
                exit;
                break;
            case "iu":
                foreach ($json['nameColume'] as $k => $value) {
                    $name .= $k . ',';
                    $val .= "'" . $value . "',";
                    $sel .= $k . "='" . $value . "' and ";
                    $upd .= $k . "='" . $value . "',";
                }

                $sel = substr($sel, 0, -5);
                $name = substr($name, 0, -1);
                $val = substr($val, 0, -1);
                $upd = substr($upd, 0, -1);

                $sql2 .= "
                    IF NOT EXISTS(SELECT TOP 1 1 FROM " . $json['nameDB'] . ".." . $json['nameTable'] . " WHERE 
                        $sel) BEGIN
                        INSERT INTO " . $json['nameDB'] . ".." . $json['nameTable'] . " (" . $name . ") VALUES(" . $val . ") \n
                    END ELSE BEGIN
                        SELECT * FROM documents..documentTheRateOf WHERE $sel
                        UPDATE  " . $json['nameDB'] . ".." . $json['nameTable'] . " SET $upd \n
                        END
                ";
                echo $sql2;
                exit;
                break;
            case "d":
                echo $sql;
                exit;
                break;
        }
    }
    /**
     * выполнение SQL скрипта
     * @param $sql
     * @param bool $errMsg
     * @param bool $returnID
     * @return bool|string
     */
    public static function exec($sql, $errMsg = true, $returnID = false)
    {
        $db = DB::getConnection();
        $result = $db->prepare('SET DATEFORMAT dmy  ' . $sql);
        try {
            if ($result->execute()) {
                if ($returnID) {
                    return $db->lastInsertId();
                }
                return true;
            }
        } catch (PDOException $Exception) {
            self::logB($sql);
            self::logB($Exception->getCode() . ":" . $Exception->getMessage());
            if ($errMsg) {
                var_dump($Exception->getCode() . ":" . $Exception->getMessage());
            }
        }
    }
}
