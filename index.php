<?php
$GLOBALS['PARAM'] = parse_ini_file('.env');
function reqFile($dir){
    foreach (scandir($dir) as $value) {
        if ($value != '.' && $value != '..') {
            require_once("$dir/$value/$value.php");
        }
    }
}
reqFile('lib');
reqFile('api');
$devKey = "7cafffc3c755da4f54441001ba918ff47ea142cb";
$saltPass = "Qq123456";
$salt = '$6$rounds=5000$'.$saltPass.'$';
$pg = qry::rout();
$class = $pg['rout'];
if (
    $class != "auth"
    && $pg['auth'] != $devKey
    && !auth::testAuth()['auth']
) header("HTTP/1.1 403 Forbidden");
else {
    if (class_exists($class)) new $class();
    else header("HTTP/1.1 404 Not Found");
}
