<?php

class passList
{
//
    private $postData;
    private $db;

    public function __construct()
    {
        $this->postData = qry::rout();//параметры get or post
        $this->db = new qry();//работа с базой
        $method = $this->postData['func'];
        if (method_exists($this, $method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        } else header("HTTP/1.1 404 Not Found");
    }

    public function getListLogin()
    {
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[mobileUserAsudd] WHERE [login]='{$this->postData['data']}'";
        return $this->db->queryGet($sql);
    }

    public function getAutoId()
    {
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[mobileTransportAsudd] WHERE [autoId]='{$this->postData['data']}'";
        return $this->db->queryGet($sql);
    }

    public function addAction()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = "INSERT INTO  ASUDD_List..mobileUserLog
           ([typeAction]
           ,[userLogin]) 
                 VALUES('{$data['typeAction']}', '{$data['userLogin']}')";
        return $this->db->queryExec($sql);
    }


    public function changeMobileLogPhoto()
    {
        $data = json_decode($this->postData['data'], true);
        $photo = "C:/ASUDDproduction/foto/";
        $arr = $data['photo'];
        $sql = "
            INSERT INTO  ASUDD_List..mobileLogPhoto
                 ( [logId]
                  ,[pathPhoto]
                  ,[namePhoto])VALUES
            ";
        foreach ($arr as $key => $value) {
            $data['pathPhoto'] = $photo . $value['path'];
            $data['namePhoto'] = $value['name'];

            $sql .= "('{$data['logId']}','{$data['pathPhoto']}','{$data['namePhoto']}')";
            $sql .= (count($arr) == $key + 1) ? ";" : ",";
        }

        return $this->db->queryExec($sql);
    }

    public function getNewTask()
    {//
        $sql2 = "SELECT * FROM [ASUDD_List].[dbo].[mobileTaskList] WHERE
 [userId]='{$this->postData['data']}'  
 AND
 (
 ([dateTime] >=cast (GETDATE() as DATE) AND (statusTask=1 OR statusTask=2 OR statusTask=3 OR statusTask=4) ) 
 OR 
 ([dateTime] <cast (GETDATE() as DATE) AND (statusTask=1 OR statusTask=2) )
 )

 ";
        return array('set' => $this->db->queryGet($sql2), 'date' => $sql2); //$this->db->queryGet($sql);
    }


    public function updateListAuto()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = '';
        $sql2 = '';
        foreach ($data as $key => $value) {
            if ($value['s'] == 'up') {
                $sql .= "UPDATE [ASUDD_List].[dbo].[mobileTransportAsudd]
                  SET [status] = '{$value['status']}'
                   WHERE [id]= '{$value['id']}';";
                $sql2 .= "INSERT INTO  ASUDD_List..mobileTransportStatusLog
([comment]
      ,[status]
      ,[autoId]
      ,[autoName]
      ,[gosNumber]
      ,[region]) VALUES ('{$value['comment']}', '{$value['status']}', '{$value['id']}','{$value['autoName']}', '{$value['gosNumber']}','{$value['region']}')
";
            }
        }
        return array('set' => $this->db->queryExec($sql), 'set2' => $this->db->queryExec($sql2));
    }

    public function changeTaskArr()
    {
        $data = json_decode($this->postData['data'], true);
        $photo = "C:/ASUDDproduction/foto/";
        $sql = '';
        $sql2 = '';

        foreach ($data as $key => $value) {
            if ($value['s'] == 'up') {
                $sql .= "UPDATE [ASUDD_List].[dbo].[mobileTaskList]
              SET [statusTask] = '{$value['statusTask']}', [title]='{$value['title']}', [description]='{$value['description']}'
              WHERE [id]= '{$value['id']}';";
                $sql2 .= "INSERT INTO  ASUDD_List..mobileTaskListLog
                 ( [comment]
      ,[statusTask]
      ,[gosNumber]
      ,[title]
      ,[description]
      ,[task]
      ,[latitude]
      ,[longitude]
      ,[userId])VALUES('{$value['comment']}','{$value['statusTask']}','{$value['gosNumber']}','{$value['title']}','{$value['description']}'
      ,'{$value['task']}','{$value['latitude']}','{$value['longitude']}','{$value['userId']}');";

                if (array_key_exists('photo', $value)) {
                    $arr = $value['photo'];
                    $sql1 = "INSERT INTO  ASUDD_List..mobileLogPhoto
                 ( [logId]
                  ,[pathPhoto]
                  ,[namePhoto])VALUES
            ";
                    foreach ($arr as $key => $val) {
                        $pathPhoto = $photo . $val['path'];
                        $namePhoto = $val['name'];
                        $sql1 .= "('{$value['id']}','{$pathPhoto}','{$namePhoto}')";
                        $sql1 .= (count($arr) == $key + 1) ? ";" : ",";
                    }
                    $this->db->queryExec($sql1);
                }
            } elseif ($value['s'] == 'ins') {
                $sql .= "INSERT INTO  ASUDD_List..mobileTaskList
([title]
      ,[description]
      ,[latitude]
      ,[longitude]
      ,[task]
      ,[statusTask]
      ,[userId]
      ,[gosNumber]
     )VALUES('{$value['title']}','{$value['description']}','{$value['latitude']}','{$value['longitude']}','{$value['task']}','{$value['statusTask']}','{$value['userId']}','{$value['gosNumber']}')";
                $sql .= ';';
                $value['comment'] = $value['comment'] ? $value['comment'] : null;
                $sql2 .= "INSERT INTO  ASUDD_List..mobileTaskListLog
                 ( [comment]
      ,[statusTask]
      ,[gosNumber]
      ,[title]
      ,[description]
      ,[task]
      ,[latitude]
      ,[longitude]
      ,[userId])VALUES('{$value['comment']}','{$value['statusTask']}','{$value['gosNumber']}','{$value['title']}','{$value['description']}'
      ,'{$value['task']}','{$value['latitude']}','{$value['longitude']}','{$value['userId']}');";
            } elseif ($value['s'] == 'del') {
                $sql .= "UPDATE [ASUDD_List].[dbo].[mobileTaskList]
              SET [show] = '1', [title]='{$value['title']}', [description]='{$value['description']}',[statusTask] = '{$value['statusTask']}'
              WHERE [id]= '{$value['id']}';";
                $sql2 .= "INSERT INTO  ASUDD_List..mobileTaskListLog
                 ( [comment]
      ,[statusTask]
      ,[gosNumber]
      ,[title]
      ,[description]
      ,[task]
      ,[latitude]
      ,[longitude]
      ,[userId])VALUES('{$value['comment']}','{$value['statusTask']}','{$value['gosNumber']}','{$value['title']}','{$value['description']}'
      ,'{$value['task']}','{$value['latitude']}','{$value['longitude']}','{$value['userId']}');";

            }
        }
        return array('set' => $this->db->queryExec($sql), 'set2' => $this->db->queryExec($sql2), 'sql2' => $sql2);
    }

    public function sendNewIncidentArr()
    {
        //  $data=$this->postData['data'];
        $data = json_decode($this->postData['data'], true);
        $comment = "test";

        foreach ($data as $key => $value) {

            $sql1 = "INSERT INTO  ASUDD_List..mobileIncidentList
           (
       [longitude]
      ,[latitude]
      ,[title]
      ,[description]
      ,[userId]
      ,[task])VALUES
      ('{$value['longitude']}','{$value['latitude']}','{$value['title']}',
       '{$value['description']}','{$value['userId']}','{$value['task']}')
       ";
            //  $id = $this->db->queryExec($sql1, [], true);
            //  $sql2 .= "('{$value['title']}','{$value['description']}','{$value['latitude']}','{$value['longitude']}','{$value['taskList']}','1','{$value['userId']}','{$value['gosNumber']}','{$id}')";
            //   $sql2 .= (count($data) == $key + 1) ? ";" : ",";
        }
        return $this->db->queryExec($sql1);
    }

    public function getListIncident()
    {//         WHERE [statusTask]='{$this->postData['data']}'
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[mobileIncidentList]";
        return $this->db->queryGet($sql);
    }

    public function getUserDate()
    {
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[mobileUserAsudd] WHERE [authId]='{$this->postData['data']}'";
        return $this->db->queryGet($sql);
    }

    public function getRegionList()
    {
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[mobileRegionList]";
        return array('set' => $this->db->queryGet($sql));
    }

    public function getAutoList()
    {
        $sql = "SELECT* FROM [ASUDD_List].[dbo].[mobileTransportAsudd] WHERE [region]='{$this->postData['data']}'";
        return $this->db->queryGet($sql);
    }

    public function getListKm()
    {
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[roadKM]    where [show] IS NULL AND ([id]!=28 AND [id]!=30 AND [id]!=32) AND nKM!=''";
        return array('set' => $this->db->queryGet($sql), 'sql' => $sql);
    }

    public function roadDev()
    {
        $sql = "SELECT  [id]
      ,[Name]
      ,[typeDeviceProjectID]
      ,[Diskr]
      ,[roadKMid]
      ,[idProject]
      ,[show]
  FROM [ASUDD_List].[dbo].[roadDevice] where roadKMid='{$this->postData['data']}' AND show IS NULL";
        return array('set' => $this->db->queryGet($sql), 'sql' => $sql);
    }

    public function getDev()
    {
        $sql = "SELECT
  t1.id
      ,t1.Name
      ,t1.typeDeviceProjectID
      ,t1.Diskr
      ,t1.roadKMid
      ,t1.idProject
	  ,t1.show
      ,t2.id as geoId
      ,t3.id as StateId
      ,t5.id as saveOptId
	  ,t4.V001 
      ,t4.V002
      ,t5.V003      
      ,t5.V007,	      
	CASE t3.F003
     WHEN 'Сервисный режим' THEN t5.V003
     ELSE t5.V007  
END as ServiceMode,
CASE t3.F003
   WHEN 'Сервисный режим' THEN 'V003'
   ELSE 'V007' 
END as ServiceModeTipe       
      
        FROM [ASUDD_List].[dbo].[roadDevice]  t1
        left JOIN [ASUDD_List].[dbo].[optionsDeviceProject] t2 ON t2.typeDeviceProjectID = t1.typeDeviceProjectID AND t1.show is NULL  AND t2.name='Геонавигация'
         left JOIN [ASUDD_List].[dbo].[optionsDeviceProject] t3 ON t3.typeDeviceProjectID = t1.typeDeviceProjectID AND  t1.show is NULL AND t3.name='Состояние'     
        left JOIN [ASUDD_List].[dbo].[saveOptionsDeviceProject] t4  ON t4.[optionsDeviceProjectID] =t2.id AND t4.roadDev = t1.id  AND t1.show is NULL 
		left JOIN [ASUDD_List].[dbo].[saveOptionsDeviceProject] t5  ON t5.[optionsDeviceProjectID] =t3.id AND t5.roadDev = t1.id  AND t1.show is NULL
		  where t1.roadKMid='{$this->postData['data']}' and t1.show is null";
        return array('set' => $this->db->queryGet($sql), 'sql' => $sql);

    }

    public function getDevices()
    {
        $sql = " SELECT 
  road.id
  
	  ,opt.diskr
	  ,opt.F001
	  ,opt.F002
	  ,opt.F003
	  ,opt.F004
	  ,opt.F005
	  ,opt.F006
	  ,opt.F007
	  ,opt.F008
	  ,opt.F009
	  ,opt.F010
	  ,opt.name
	  ,opt.id as optId
	  ,sav.optionsDeviceProjectID
	   ,sav.id as saveId 

	  ,sav.V001
	  ,sav.V002
	  ,sav.V003
	  ,sav.V004
	  ,sav.V005
	  ,sav.V006
	  ,sav.V007
	  ,sav.V008
	  ,sav.V009
	  ,sav.V010 

  FROM [ASUDD_List].[dbo].[roadDevice] road    
  left JOIN [ASUDD_List].[dbo].[optionsDeviceProject] opt ON opt.typeDeviceProjectID = road.typeDeviceProjectID AND road.show is NULL AND opt.show is NULL
   left JOIN [ASUDD_List].[dbo].[saveOptionsDeviceProject] sav ON opt.id = sav.optionsDeviceProjectID AND sav.roadDev= road.id AND road.show is NULL AND opt.show is NULL

   where road.show is NULL AND road.roadKMid='{$this->postData['data']}'";
        return array('set' => $this->db->queryGet($sql), 'sql' => $sql);
    }


    public function updateNavigation()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = '';
        foreach ($data as $key => $value) {
            if ($value['saveId']) {
                $sql .= "UPDATE [ASUDD_List].[dbo].[saveOptionsDeviceProject]
              SET [V001] = '{$value['latitude']}', [V002] = '{$value['longitude']}'
              WHERE [roadDev]= '{$value['id']}' AND [optionsDeviceProjectID]='{$value['optId']}';";
            } else {
                $sql .= "INSERT INTO  ASUDD_List..saveOptionsDeviceProject
([V001],[V002],  [optionsDeviceProjectID],[roadDev]) VALUES ('{$value['latitude']}', '{$value['longitude']}','{$value['optId']}','{$value['id']}');";
            }
        }
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }

    public function updateServiceMode()
    {
        $data = json_decode($this->postData['data'], true);
        if ($data['s'] == 'upd') {
            $sql = "UPDATE [ASUDD_List].[dbo].[saveOptionsDeviceProject]
            SET [{$data['ServiceModeTipe']}]='{$data['ServiceMode']}'
            WHERE [id]='{$data['saveId']}'";
        } elseif ($data['s'] == 'ins') {

            $sql = "INSERT INTO  ASUDD_List..saveOptionsDeviceProject
           (
           [optionsDeviceProjectID]
           ,[roadDev]           
           ,[{$data['ServiceModeTipe']}]
            ) VALUES
            (
            '{$data['optId']}',
            '{$data['id']}',
            '{$data['ServiceMode']}'
             )";
        }
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }

    public function deleteDevice()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = "UPDATE  [ASUDD_List].[dbo].[roadDevice]
        SET [show]='0' WHERE [id]=$data[id]";
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }

    public function getTypeDevices()
    {
        $sql = "SELECT*FROM [ASUDD_List].[dbo].[typeDeviceProject] WHERE [show] IS NULL AND [id]!='1030' AND [id]!='12' AND [id]!='13'";
        return $this->db->queryGet($sql);
    }

    public function addDevices()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = "INSERT INTO  ASUDD_List..roadDevice
([Name]
      ,[typeDeviceProjectID]
      ,[Diskr]
      ,[roadKMid]
      ,[idProject]
      ,[show])
      VALUES (
      '{$data['Name']}',
      '{$data['typeDeviceProjectID']}',
       '{$data['Diskr']}',
       '{$data['roadKMid']}',
         '{$data['idProject']}',
         NULL)";

        return array('set' => $this->db->queryExec($sql, [], true), 'sql' => $sql);
    }

    public function changeNameKm()
    {
        $data = json_decode($this->postData['data'], true);

        $sql = "UPDATE  [ASUDD_List].[dbo].[roadKM]
       SET [name]=N'{$data['name']}' WHERE [id]='{$data['id']}'";
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }

    public function changeNameDev()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = "UPDATE  [ASUDD_List].[dbo].[roadDevice]
        SET [Name]=N'{$data['Name']}' WHERE [id]='{$data['id']}'";
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }

    public function changeOptions()
    {
        $data = json_decode($this->postData['data'], true);
        $sql = '';
        if ($data['saveId']) {
            foreach ($data['opt'] as $key => $value) {
                $sql .= "UPDATE [ASUDD_List].[dbo].[saveOptionsDeviceProject]
            SET [{$value['optParam']}]=N'{$value['opt']}'
            WHERE [id]='{$data['saveId']}';";
            }
        } else {
            $params = "";
            $paramsVal = "";

            foreach ($data['opt'] as $key => $value) {
                $params .= " [{$value['optParam']}]";
                $paramsVal .= "'{$value['opt']}'";
                $params .= (count($data['opt']) == $key + 1) ? "" : ",";
                $paramsVal .= (count($data['opt']) == $key + 1) ? "" : ",";
            }
            $sql .= "INSERT INTO  ASUDD_List..saveOptionsDeviceProject([optionsDeviceProjectID]
           ,[roadDev],$params    ) VALUES    (
            '{$data['optId']}',
            '{$data['id']}',
             $paramsVal
             )";
        }
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }

    public function addDevActionLog()
    {   $data = json_decode($this->postData['data'], true);
        $data['userApp']=json_encode($data['userApp'], JSON_UNESCAPED_UNICODE);
        $data['deviceId']=$data['deviceId']?$data['deviceId']:NULL;
        $sql = "INSERT INTO  ASUDD_List..mobileDeviceActionsLog
      ([actionDevValue]
      ,[userApp]
      ,[gosNumber]
      ,[kmId]
      ,[deviceId]
      ,[prevValue])
      VALUES (
      '{$data['actionDevValue']}',
      '{$data['userApp']}',
       '{$data['gosNumber']}',
       '{$data['kmId']}',
        '{$data['deviceId']}',
        '{$data['prevValue']}')";
        return array('set' => $this->db->queryExec($sql), 'sql' => $sql);
    }
}


