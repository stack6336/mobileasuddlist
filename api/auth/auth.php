<?php
class auth {
    private $pg;
    function __construct(){
        $this->pg = qry::rout();
        $method = $this->pg['func'];
        if(method_exists($this,$method)) {
            $res = $this->$method();
            echo is_array($res) ? json_encode($res) : $res;
        }else header("HTTP/1.1 404 Not Found");
    }

    static function testAuth(){
        foreach(explode("&",explode("?",$_SERVER['REQUEST_URI'])[1]) as $val){
            $e = explode("=",$val);
            $_GET[$e[0]]=str_replace("#plus#", "+", urldecode(str_replace("+", "#plus#", $e[1])));
        }
        $pg = (!empty($_POST)) ? $_POST : $_GET;
        $auth = json_decode($pg['auth']);
        global $salt;
        $hash = explode("$",crypt(json_encode($auth->info,JSON_UNESCAPED_UNICODE).json_encode($auth->access,JSON_UNESCAPED_UNICODE), $salt))[4];
        return array('auth'=>$auth->hash==$hash);
    }
}